{% docs zuora_accounts_new_updated %}
Base counts along with day names for accounts, used in Periscope queries to work out daily averages and report on Zuora data

{% enddocs %}
{% docs zuora_subscriptions_new_updated %}
Base counts along with day names for subscriptions, used in Periscope queries to work out daily averages and report on Zuora data

{% enddocs %}
